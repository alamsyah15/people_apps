// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:people_apps/core/utils/constant.dart';
import 'package:people_apps/core/utils/db_helper.dart';
import 'package:people_apps/core/viewmodels/auth/auth_provider.dart';
import 'package:people_apps/core/viewmodels/favorite/favorite_provider.dart';
import 'package:people_apps/core/viewmodels/home/detail_people_provider.dart';
import 'package:people_apps/core/viewmodels/home/navigation_provider.dart';
import 'package:people_apps/core/viewmodels/home/people_provider.dart';
import 'package:people_apps/screen/home/home_navigation.dart';
import 'package:people_apps/screen/home/home_screen.dart';
import 'package:provider/provider.dart';

import 'screen/authentication/signin_screen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await DbHelper().initDb(); // Create table to local db
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => NavigationProvider()),
        ChangeNotifierProvider(create: (_) => PeopleProvider()),
        ChangeNotifierProvider(create: (_) => DetailPeopleProvider()),
        ChangeNotifierProvider(create: (_) => AuthProvider()),
        ChangeNotifierProvider(create: (_) => FavoriteProvider()),
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        navigatorKey: navigatorKey,
        theme: ThemeData.dark(),
        home: SigninScreen(),
      ),
    );
  }
}
