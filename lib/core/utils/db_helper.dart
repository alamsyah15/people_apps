// ignore_for_file: avoid_print

import 'package:people_apps/core/services/log.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart';
import 'dart:async';

class DbHelper {
  static Database? _database;
  Future<Database?> get database async {
    _database = await initDb();
    return _database;
  }

  Future<Database> initDb() async {
    var directory = await getApplicationDocumentsDirectory();
    String path = join(directory.path, "people-apps.db");

    return openDatabase(path, version: 1, onCreate: onCreateDb);
  }

  // Create Table
  void onCreateDb(Database db, int version) async {
    await createRequireTb(db);
  }

  Future createRequireTb(Database db) async {
    // Create All Table
    await createUserTb(db);
    await createFavoriteTb(db);
    await createPeopleTb(db);
    await createSpeciesTb(db);
    await createFilmTb(db);
  }

  // =======> Db Table Configurator <======
  Future createUserTb(Database db) async {
    try {
      await db.execute('''
      CREATE TABLE users (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        name TEXT,
        email TEXT,
        password TEXT
        )
    ''');
    } catch (e) {
      Log.e("Exception Table users Create $e");
    }
  }

  Future createFavoriteTb(Database db) async {
    try {
      await db.execute('''
      CREATE TABLE favorite (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        people_id TEXT,
        users_id INTEGER
        )
    ''');
    } catch (e) {
      Log.e("Exception Table favorite Create $e");
    }
  }

  Future createPeopleTb(Database db) async {
    try {
      await db.execute('''
      CREATE TABLE people (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        name TEXT,
        height TEXT,
        mass TEXT,
        hair_color TEXT,
        skin_color TEXT,
        eye_color TEXT,
        birth_year TEXT,
        gender TEXT,
        films TEXT,
        species TEXT,
        url TEXT
        )
    ''');
    } catch (e) {
      Log.e("Exception Table people Create $e");
    }
  }

  Future createSpeciesTb(Database db) async {
    try {
      await db.execute('''
      CREATE TABLE species (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        name TEXT,
        classification TEXT,
        designation TEXT,
        average_height TEXT,
        skin_colors TEXT,
        hair_colors TEXT,
        eye_colors TEXT,
        url TEXT
        )
    ''');
    } catch (e) {
      Log.e("Exception Table species Create $e");
    }
  }

  Future createFilmTb(Database db) async {
    try {
      await db.execute('''
      CREATE TABLE film (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        title TEXT,
        episode_id TEXT,
        opening_crawl TEXT,
        director TEXT,
        producer TEXT,
        url TEXT
        )
    ''');
    } catch (e) {
      Log.e("Exception Table film Create $e");
    }
  }
}
