import 'package:flutter/material.dart';
import 'package:people_apps/core/utils/constant.dart';

Future pushTo(Widget widget) async {
  return Navigator.of(navigatorKey.currentContext!)
      .push(MaterialPageRoute(builder: (_) => widget));
}

Future pushBack([dynamic argument]) async {
  return Navigator.pop(navigatorKey.currentContext!, argument);
}

Future pushReplacement(Widget widget) async {
  return Navigator.of(navigatorKey.currentContext!)
      .pushReplacement(MaterialPageRoute(builder: (_) => widget));
}

Future pushRemoveUntil(Widget widget) async {
  return Navigator.of(navigatorKey.currentContext!).pushAndRemoveUntil(
      MaterialPageRoute(builder: (_) => widget), (route) => false);
}
