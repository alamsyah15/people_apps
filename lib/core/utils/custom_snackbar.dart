// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:people_apps/core/utils/constant.dart';

void errorSnackBar(dynamic error) {
  ScaffoldMessenger.of(navigatorKey.currentContext!).showSnackBar(
    SnackBar(
      content: Row(
        children: [
          Icon(Icons.error_outline, color: Colors.red),
          SizedBox(width: 16),
          Expanded(child: Text(error.toString())),
        ],
      ),
    ),
  );
}

void infoActionSnackBar({
  required BuildContext context,
  required String message,
  required String label,
  required Function() onClick,
}) {
  ScaffoldMessenger.of(context).showSnackBar(
    SnackBar(
      content: Row(
        children: [
          Icon(Icons.info_outline, color: Colors.grey),
          SizedBox(width: 16),
          Expanded(child: Text(message)),
        ],
      ),
      action: SnackBarAction(
        label: label,
        onPressed: onClick,
        textColor: Colors.red,
      ),
    ),
  );
}
