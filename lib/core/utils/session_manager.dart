// ignore_for_file: prefer_const_constructors

import 'dart:convert';
import 'dart:developer';

import 'package:people_apps/core/model/user_model.dart';
import 'package:people_apps/core/utils/navigation_helper.dart';
import 'package:people_apps/screen/authentication/signin_screen.dart';
import 'package:people_apps/screen/home/home_navigation.dart';
import 'package:shared_preferences/shared_preferences.dart';

const String LOGIN_SESSION = "account";

class SessionManager {
  static Future<SharedPreferences> prefs = SharedPreferences.getInstance();

  static Future saveSessionLogin(String value) async {
    final session = await prefs;
    session.setString("account", value);
    pushReplacement(HomeNavigation());
  }

  static Future checkSession() async {
    final session = await prefs;
    String? data = session.getString("account");
    if (data != null) {
      pushReplacement(HomeNavigation());
      return UserModel.fromJson(jsonDecode(data));
    }
  }

  static Future removeSessionLogin() async {
    final session = await prefs;
    session.remove(LOGIN_SESSION);
    pushRemoveUntil(SigninScreen());
  }

  // Set Bool Session
  static Future setBool(String key, bool value) async {
    final session = await prefs;
    return await session.setBool(key, value);
  }

  // Get Bool Session
  static Future getBool(String key) async {
    final session = await prefs;
    return session.getBool(key);
  }

  // Set String Session
  static Future setString(String key, String value) async {
    final session = await prefs;
    return await session.setString(key, value);
  }

  // Get String Session
  static Future getString(String key) async {
    final session = await prefs;
    return session.getString(key);
  }

  // Set Integer Session
  static Future setInteger(String key, int value) async {
    final session = await prefs;
    return await session.setInt(key, value);
  }

  // Get String Session
  static Future getInteger(String key) async {
    final session = await prefs;
    return session.getInt(key);
  }

  // Set String List Session
  static Future setStringList(String key, List<String> value) async {
    final session = await prefs;
    return await session.setStringList(key, value);
  }

  // Get String List Session
  static Future getStringList(String key) async {
    final session = await prefs;
    return session.getStringList(key);
  }

  // Remove Value
  static Future remove(String key) async {
    final session = await prefs;
    return await session.remove(key);
  }
}
