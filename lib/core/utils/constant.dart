// ignore_for_file: prefer_const_declarations, prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:people_apps/screen/favorite/favorite_screen.dart';
import 'package:people_apps/screen/home/home_screen.dart';
import 'package:people_apps/screen/profile/profile_screen.dart';

/// Environment Url
final String development = "https://swapi.dev/api";
final String production = "";

/// Mapping Url
final String baseUrl = development;

/// List Filter
List<String> listFilter = ["Sort by A-Z", "Sort by Z-A"];

/// Global Key Route
GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

/// Size Config
double get widthScreen =>
    MediaQuery.of(navigatorKey.currentContext!).size.width;
double get heightScreen =>
    MediaQuery.of(navigatorKey.currentContext!).size.height;

/// List Nav Bar
List<BottomNavigationBarItem> listNavMenu = [
  BottomNavigationBarItem(
    icon: Icon(Icons.home),
    label: "Beranda",
  ),
  BottomNavigationBarItem(
    icon: Icon(Icons.bookmark),
    label: "Favorite",
  ),
  BottomNavigationBarItem(
    icon: Icon(Icons.person),
    label: "Profile",
  ),
];

List<Widget> listScreen = [
  HomeScreen(),
  FavoriteScreen(),
  ProfileScreen(),
];
