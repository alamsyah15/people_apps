// ignore_for_file: prefer_final_fields

import 'dart:convert';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:people_apps/core/model/user_model.dart';
import 'package:people_apps/core/services/auth/auth_services.dart';
import 'package:people_apps/core/services/log.dart';
import 'package:people_apps/core/utils/custom_snackbar.dart';
import 'package:people_apps/core/utils/navigation_helper.dart';
import 'package:people_apps/core/utils/session_manager.dart';

class AuthProvider extends ChangeNotifier {
  // =====> Property <=======
  bool _isLoading = false;
  bool hidePassword = true;
  UserModel? _user;

  UserModel? get user => _user;

  bool get isLoading => _isLoading;
  set isLoading(bool val) {
    _isLoading = val;
    notifyListeners();
  }

  setHidePassword([bool? value]) {
    hidePassword = value ?? !hidePassword;
    notifyListeners();
  }

  // =====> Method <=======
  void login(Map<String, dynamic> requestBody) async {
    isLoading = true;
    final res = await AuthServices.signIn(requestBody);
    isLoading = false;

    if (res is UserModel) {
      SessionManager.saveSessionLogin(jsonEncode(res.toJson()));
      _user = res;
    } else {
      errorSnackBar(res);
    }
  }

  void register(Map<String, dynamic> requestBody) async {
    final res = await AuthServices.signUp(requestBody);
    if (res is Map<String, dynamic>) {
      pushBack();
      login(res);
    } else {
      errorSnackBar(res);
    }
  }

  void checkSession() async {
    final result = await SessionManager.checkSession();
    if (result != null) {
      _user = result;
      notifyListeners();
    }
  }
}
