// ignore_for_file: prefer_final_fields, prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:people_apps/screen/home/home_screen.dart';

class NavigationProvider extends ChangeNotifier {
  // =====> Property <=======
  bool _isGridView = false;
  int _currentIndex = 0;
  Widget _displayScreen = HomeScreen();

  bool get isGridView => _isGridView;
  set isGridView(bool val) {
    _isGridView = val;
    notifyListeners();
  }

  Widget get displayScreen => _displayScreen;

  int get currentIndex => _currentIndex;
  set currentIndex(int val) {
    _currentIndex = val;
    notifyListeners();
  }

  // =======> Method <========
  void setDisplay(Widget display) {
    _displayScreen = display;
    notifyListeners();
  }
}
