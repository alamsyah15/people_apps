import 'package:flutter/material.dart';
import 'package:people_apps/core/model/film_model.dart';
import 'package:people_apps/core/model/people_model.dart';
import 'package:people_apps/core/model/spesies_model.dart';
import 'package:people_apps/core/services/home/people_service.dart';
import 'package:people_apps/core/utils/constant.dart';
import 'package:people_apps/core/viewmodels/auth/auth_provider.dart';
import 'package:provider/provider.dart';

class DetailPeopleProvider extends ChangeNotifier {
  // =====> Property <=======

  SpesiesModel? spesiesData;
  List<FilmModel> listFilm = [];
  bool isLoading = false;
  bool isFavorite = false;
  final authProv =
      Provider.of<AuthProvider>(navigatorKey.currentContext!, listen: false);

  // =====> Method Init <======

  void initialized(Result people) async {
    reset();
    // Set init favorite
    getFavorite(people);

    // Fetch data spesies when spesies length not empty
    if (people.species != null && (people.species?.length ?? 0) != 0) {
      isLoading = true;
      await getSpesies(people.species!.first);
      isLoading = false;
    }

    // Fetch data film when film length not empty
    if (people.films != null && (people.species?.length ?? 0) != 0) {
      for (int i = 0; i < people.films!.length; i++) {
        final result = await getFilm(people.films![i]);
        if (result is FilmModel) {
          listFilm.add(result);
        }
        notifyListeners();
      }
    }
  }

  // =====> Method <=======
  Future getFilm(String url) async {
    final res = await PeopleService.getFilm(url);
    if (res is FilmModel) {
      PeopleService.insertFilmLocal(res);
      return res;
    }
    notifyListeners();
  }

  Future getSpesies(String url) async {
    final res = await PeopleService.getSpesies(url);
    if (res is SpesiesModel) {
      spesiesData = res;
    }
    if (spesiesData != null) {
      PeopleService.insertSpeciesLocal(spesiesData!);
    }
    notifyListeners();
  }

  Future getFavorite(Result people) async {
    final user = authProv.user;
    final resultFav = await PeopleService.getFavorite();
    if (resultFav is List<Map<String, dynamic>>) {
      final sortListFav = resultFav
          .where(
              (e) => e['people_id'] == people.url && e['users_id'] == user!.id)
          .toList();

      if (sortListFav.isNotEmpty) {
        isFavorite = true;
        notifyListeners();
      }
    }
  }

  Future setFavorite(String url) async {
    final user = authProv.user;
    Map<String, dynamic> requestBody = {
      'people_id': url,
      'users_id': user!.id,
    };
    if (isFavorite == true) {
      final result = await PeopleService.unFavorite(requestBody);
      if (result == true) {
        isFavorite = false;
      }
    } else {
      final result = await PeopleService.insertFavorite(requestBody);
      if (result == true) {
        isFavorite = true;
      }
    }
    notifyListeners();
  }

  void reset() {
    spesiesData = null;
    listFilm = [];
    isFavorite = false;
  }
}
