// ignore_for_file: prefer_final_fields

import 'package:flutter/material.dart';
import 'package:people_apps/core/model/people_model.dart';
import 'package:people_apps/core/services/home/people_service.dart';
import 'package:people_apps/core/utils/custom_snackbar.dart';
import 'package:people_apps/core/utils/db_helper.dart';
import 'package:people_apps/core/utils/navigation_helper.dart';

class PeopleProvider extends ChangeNotifier {
  // ======>  Property <======
  bool isLoading = false;
  bool isLoadMore = false;
  PeopleModel? peopleModel;
  List<Result> listPeople = [];
  List<Result> listPeopleLocal = [];
  List<Result> bckupListPeople = [];
  List<Result> bckupListPeopleLocal = [];

  int page = 1;
  int valueFilter = 0;
  ScrollController scController = ScrollController();

  // =====> Method Init <======

  void initialized() async {
    reset();
    isLoading = true;
    try {
      await getPeople();
    } catch (e) {}
    await getPeopleLocal();

    isLoading = false;
    notifyListeners();
    filterBy(0);

    scController.addListener(() async {
      if (scController.position.pixels ==
          scController.position.maxScrollExtent) {
        // checking data when max pagginate
        if (peopleModel?.next != null) {
          // action to load more data
          isLoadMore = true;
          notifyListeners();
          await getPeople();
          await getPeopleLocal();
        }
      }
    });
  }

  // =====> Method <======
  Future getPeople() async {
    if (isLoadMore) page++;

    final res = await PeopleService.getPeople(page);
    if (res is PeopleModel) {
      peopleModel = res;
      if (isLoadMore) {
        listPeople.addAll(res.results ?? []);
        bckupListPeople.addAll(res.results ?? []);
      } else {
        listPeople = res.results ?? [];
        bckupListPeople.addAll(res.results ?? []);
      }
    }
    await PeopleService.insertPeopleLocal(listPeople); // Insert people to local
    isLoadMore = false;
    notifyListeners();
  }

  Future getPeopleLocal() async {
    bckupListPeopleLocal = [];
    final res = await PeopleService.getPeopleLocal();
    if (res is List<Result>) {
      listPeopleLocal = res;
      bckupListPeopleLocal.addAll(listPeopleLocal);
      notifyListeners();
      if (res.isEmpty) {
        return false;
      }
      return true;
    }
    return false;
  }

  Future insertPeople(Result people) async {
    final res = await PeopleService.insertPeopleLocal([people]);
    if (res == true) {
      pushBack();
      initialized();
    }
  }

  Future updatePeople(Result people) async {
    final res = await PeopleService.updatePeopleLocal([people]);
    if (res == true) {
      pushBack();
      initialized();
    }
  }

  Future deletePeople(Result people) async {
    final res = await PeopleService.deletePeopleLocal(people);
    if (res == true) {
      pushBack();
      initialized();
    }
  }

  void reset() {
    listPeople = [];
    bckupListPeople = [];
    listPeopleLocal = [];
    bckupListPeopleLocal = [];

    page = 1;
  }

  void search(String query) {
    listPeople = bckupListPeople;
    listPeopleLocal = bckupListPeopleLocal;
    if (query.isNotEmpty) {
      query = query.toLowerCase();
      listPeople = listPeople
          .where((e) => e.name!.toLowerCase().contains(query))
          .toList();
      listPeopleLocal = listPeopleLocal
          .where((e) => e.name!.toLowerCase().contains(query))
          .toList();
    }
    notifyListeners();
  }

  void filterBy(int value) {
    valueFilter = value;

    if (valueFilter == 0) {
      listPeople.sort((a, b) => a.name!.compareTo(b.name!));
      listPeopleLocal.sort((a, b) => a.name!.compareTo(b.name!));
    } else {
      listPeople.sort((a, b) => b.name!.compareTo(a.name!));
      listPeopleLocal.sort((a, b) => b.name!.compareTo(a.name!));
    }

    notifyListeners();
  }
}
