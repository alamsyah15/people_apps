import 'package:flutter/material.dart';
import 'package:people_apps/core/model/people_model.dart';
import 'package:people_apps/core/services/favorite/favorite_service.dart';

class FavoriteProvider extends ChangeNotifier {
  // =====> Property <=======
  List<Result> listPeople = [];
  bool isLoading = false;

  // =====> Method Init <=======
  void initilized() async {
    listPeople = [];
    isLoading = true;
    await getFavorite();
    isLoading = false;
  }

  // =====> Method <=======

  Future getFavorite() async {
    final result = await FavoriteService.getFavorite();
    if (result is List<Result>) {
      listPeople = result;
    }
    notifyListeners();
  }
}
