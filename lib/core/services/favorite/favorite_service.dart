import 'dart:convert';

import 'package:people_apps/core/model/people_model.dart';
import 'package:people_apps/core/services/log.dart';
import 'package:people_apps/core/utils/db_helper.dart';
import 'package:sqflite/sqflite.dart';

class FavoriteService {
// ======> Get Favorite <=======
  static Future getFavorite() async {
    // try {
    Database? db = await DbHelper().database;

    // Get Existing data
    if (db?.isOpen == true) {
      List<Map<String, Object?>> result = await db!.query("favorite");
      List<Result> tempListResult = [];
      for (int i = 0; i < result.length; i++) {
        final resPeople = await db.rawQuery(
          'SELECT * FROM people WHERE url=?',
          ['${result[i]['people_id']}'],
        );
        final listPeople =
            List<Result>.from(resPeople.map((e) => Result.fromJson(e)));
        tempListResult.addAll(listPeople);
      }

      Log.v("Favorite Local $tempListResult");
      return tempListResult;
    }
    // } catch (e) {
    //   Log.e("Exception PeopleService.getFavorite : $e");
    // }
  }
}
