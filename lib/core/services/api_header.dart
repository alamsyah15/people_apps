// ignore_for_file: curly_braces_in_flow_control_structures, unnecessary_null_comparison, prefer_final_fields

import 'package:dio/dio.dart';

Dio dio = Dio();

class ApiHeader {
  static final _apiHeader = ApiHeader._();
  var _headers = {
    "Accept": "application/json",
    "Content-Type": "application/json"
  };

  ApiHeader._();
  factory ApiHeader() => _apiHeader;
  Map<String, String> get header => _headers;

  void applyHeader(String name, String value) {
    _headers[name] = value;
  }
}

ApiHeader apiHeader = ApiHeader();
