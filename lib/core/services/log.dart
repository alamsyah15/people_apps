// ignore_for_file: curly_braces_in_flow_control_structures, unnecessary_null_comparison

import 'dart:convert';
import 'dart:developer';
import 'package:dio/dio.dart';

class Log {
  static const app = "PEOPLE APPS";
  static const isLogBody = false;

  static v(String message) {
    log(message, time: DateTime.now(), name: app);
  }

  static e(String label, [dynamic error]) {
    if (error is DioError) {
      log('',
          time: DateTime.now(), name: app, error: "[ERROR] ${error.message}");
      log('',
          time: DateTime.now(),
          name: app,
          error: "[ERROR] ${error.response?.realUri}");
    } else {
      log('', time: DateTime.now(), name: app, error: "[ERROR] $label");
      log('', time: DateTime.now(), name: app, error: "[ERROR] $error");
    }
  }

  static http(Response response, Map params, [bool isBody = false]) {
    if (response.statusCode == 200) {
      log('HTTP   : ${response.realUri}', time: DateTime.now(), name: app);
      log('Status : ${response.statusCode} | ${response.statusMessage}',
          time: DateTime.now(), name: app);
      if (params != null)
        log('Request  => $params', time: DateTime.now(), name: app);
      if (isBody)
        log('Response => ${jsonEncode(response.data)}',
            time: DateTime.now(), name: app);
    } else {
      log('',
          time: DateTime.now(),
          name: app,
          error: '[ERROR] HTTP   : ${response.realUri}');
      log('',
          time: DateTime.now(),
          name: app,
          error:
              '[ERROR] Status : ${response.statusCode} | ${response.statusMessage}');
      if (params != null)
        log('',
            time: DateTime.now(),
            name: app,
            error: '[ERROR] Request  => $params');
      log('',
          time: DateTime.now(),
          name: app,
          error: '[ERROR] Response => ${response.data}');
    }
  }
}
