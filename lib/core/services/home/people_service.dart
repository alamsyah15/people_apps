// ignore_for_file: avoid_function_literals_in_foreach_calls, use_rethrow_when_possible

import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:people_apps/core/model/film_model.dart';
import 'package:people_apps/core/model/people_model.dart';
import 'package:people_apps/core/model/spesies_model.dart';
import 'package:people_apps/core/services/api_header.dart';
import 'package:people_apps/core/services/log.dart';
import 'package:people_apps/core/utils/constant.dart';
import 'package:people_apps/core/utils/db_helper.dart';
import 'package:provider/provider.dart';
import 'package:sqflite/sqflite.dart';

class PeopleService {
  // =====> Get People <======
  static Future getPeople(int page) async {
    Log.v("Page $page");
    try {
      final param = {'page': '$page', 'format': 'json'};
      Response res = await dio.get(
        "$baseUrl/people",
        queryParameters: param,
        options: Options(headers: apiHeader.header),
      );
      Log.http(res, param, true);
      if (res.statusCode == 200) {
        return PeopleModel.fromJson(res.data);
      }
    } on Exception catch (e) {
      Log.e("Error PeopleService.getPeople =>  $e");
      throw e.toString();
    }
  }

  // =====> Get Spesies <======
  static Future getSpesies(String url) async {
    final param = {'format': 'json'};
    try {
      Response res = await dio.get(
        url,
        queryParameters: param,
        options: Options(headers: apiHeader.header),
      );
      Log.http(res, param, true);
      if (res.statusCode == 200) {
        return SpesiesModel.fromJson(res.data);
      }
    } on Exception catch (e) {
      Log.e("Error PeopleService.getSpesies =>  $e");
      throw e.toString();
    }
  }

  // =====> Get Film <======
  static Future getFilm(String url) async {
    final param = {'format': 'json'};
    try {
      Response res = await dio.get(
        url,
        queryParameters: param,
        options: Options(headers: apiHeader.header),
      );
      Log.http(res, param, true);
      if (res.statusCode == 200) {
        return FilmModel.fromJson(res.data);
      }
    } on Exception catch (e) {
      Log.e("Error PeopleService.getFilm =>  $e");
      throw e.toString();
    }
  }

  // ?==================> Local Services <==========================?

  // =====> Get People from Db Local <=====
  static Future getPeopleLocal() async {
    try {
      Database? db = await DbHelper().database;
      // db!.delete('people');
      // Get Existing data
      List<Map<String, Object?>> result = await db!.query("people");
      Log.v("People Local ${jsonEncode(result)}");
      List<Result> tempListResult = [];
      if (result.isNotEmpty) {
        result.forEach((json) => tempListResult.add(Result.fromJson(json)));
      }

      return tempListResult;
    } catch (e) {
      Log.e("Exception PeopleService.getPeopleLocal : $e");
    }
  }

  // =====> Get Species from Db Local <=====
  static Future getSpeciesLocal() async {
    try {
      Database? db = await DbHelper().database;

      // Get Existing data
      List<Map<String, Object?>> result = await db!.query("species");
      Log.v("Species Local ${jsonEncode(result)}");
      List<SpesiesModel> tempListSpecies = [];
      if (result.isNotEmpty) {
        result.forEach(
          (json) => tempListSpecies.add(SpesiesModel.fromJson(json)),
        );
      }

      return tempListSpecies;
    } catch (e) {
      Log.e("Exception PeopleService.getSpeciesLocal : $e");
    }
  }

  // =====> Get Film from Db Local <=====
  static Future getFilmLocal() async {
    try {
      Database? db = await DbHelper().database;

      // Get Existing data
      List<Map<String, Object?>> result = await db!.query("film");
      Log.v("Film Local ${jsonEncode(result)}");
      List<FilmModel> tempListSpecies = [];
      if (result.isNotEmpty) {
        result.forEach(
          (json) => tempListSpecies.add(FilmModel.fromJson(json)),
        );
      }

      return tempListSpecies;
    } catch (e) {
      Log.e("Exception PeopleService.getFilmLocal : $e");
    }
  }

  // =====> Get Favorite from Db Local <=====
  static Future getFavorite() async {
    try {
      Database? db = await DbHelper().database;

      // Get Existing data
      List<Map<String, Object?>> result = await db!.query("favorite");
      Log.v("Favorite Local ${jsonEncode(result)}");
      return result;
    } catch (e) {
      Log.e("Exception PeopleService.getFavorite : $e");
    }
  }

  // =====> Insert People to Db Local <=====
  static Future insertPeopleLocal(List<Result> listPeople) async {
    try {
      Log.v("Length People ${listPeople.length}");
      Database? db = await DbHelper().database;
      // db!.delete('people');
      // Get Existing data
      List<Result> tempListResult = await getPeopleLocal();

      // Insert Data to local
      if (db!.isOpen && listPeople.isNotEmpty) {
        for (int i = 0; i < listPeople.length; i++) {
          Result people = listPeople[i];
          final tempPeople =
              tempListResult.where((e) => e.url == people.url).toList();
          if (tempPeople.isEmpty) {
            Map<String, dynamic> requestBody = people.toJson();
            requestBody['films'] = jsonEncode(requestBody['films']);
            requestBody['species'] = jsonEncode(requestBody['species']);
            requestBody.remove('homeworld');
            requestBody.remove('vehicles');
            requestBody.remove('starships');
            requestBody.remove('created');
            requestBody.remove('edited');
            await db.insert(
              'people',
              requestBody,
              nullColumnHack: null,
            );
          }
        }
        return true;
      } else {
        return false;
      }
    } catch (e) {
      Log.e("Exception PeopleService.insertPeopleLocal : $e");
    }
  }

  // =====> Update People to Db Local <=====
  static Future updatePeopleLocal(List<Result> listPeople) async {
    try {
      Database? db = await DbHelper().database;

      // Insert Data to local
      if (db!.isOpen && listPeople.isNotEmpty) {
        for (int i = 0; i < listPeople.length; i++) {
          Result people = listPeople[i];
          Map<String, dynamic> requestBody = people.toJson();
          requestBody['films'] = jsonEncode(requestBody['films']);
          requestBody['species'] = jsonEncode(requestBody['species']);
          requestBody.remove('homeworld');
          requestBody.remove('vehicles');
          requestBody.remove('starships');
          requestBody.remove('created');
          requestBody.remove('edited');

          final res = await db.update(
            'people',
            requestBody,
            where: 'url=?',
            whereArgs: ['${people.url}'],
          );
          return true;
        }
      } else {
        return false;
      }
    } catch (e) {
      Log.e("Exception PeopleService.insertPeopleLocal : $e");
    }
  }

  // =====> Delete People to Db Local <=====
  static Future deletePeopleLocal(Result people) async {
    try {
      Database? db = await DbHelper().database;

      // Insert Data to local
      if (db!.isOpen) {
        final res = await db.delete(
          'people',
          where: 'url=?',
          whereArgs: ['${people.url}'],
        );
        return true;
      } else {
        return false;
      }
    } catch (e) {
      Log.e("Exception PeopleService.insertPeopleLocal : $e");
    }
  }

  // =====> Insert Species to Db Local <=====
  static Future insertSpeciesLocal(SpesiesModel spesies) async {
    try {
      Database? db = await DbHelper().database;
      List<SpesiesModel> tempListSpecies = await getSpeciesLocal();

      // Insert Data to local
      if (db!.isOpen) {
        final tempSpecies =
            tempListSpecies.where((e) => e.url == spesies.url).toList();
        if (tempSpecies.isEmpty) {
          Map<String, dynamic> requestBody = spesies.toJson();
          requestBody.remove('homeworld');
          requestBody.remove('language');
          requestBody.remove('average_lifespan');
          requestBody.remove('people');
          requestBody.remove('films');
          requestBody.remove('created');
          requestBody.remove('edited');
          db.insert(
            'species',
            requestBody,
            nullColumnHack: null,
          );
        }
      }
    } catch (e) {
      Log.e("Exception PeopleService.insertSpeciesLocal : $e");
    }
  }

  // =====> Insert Film to Db Local <=====
  static Future insertFilmLocal(FilmModel film) async {
    try {
      Database? db = await DbHelper().database;
      // db!.delete('film');
      // Get Existing data
      List<FilmModel> tempListFilm = await getFilmLocal();

      // Insert Data to local
      if (db!.isOpen) {
        final tempFilm = tempListFilm.where((e) => e.url == film.url).toList();
        if (tempFilm.isEmpty) {
          Map<String, dynamic> requestBody = film.toJson();
          requestBody.remove('release_date');
          requestBody.remove('characters');
          requestBody.remove('planets');
          requestBody.remove('starships');
          requestBody.remove('vehicles');
          requestBody.remove('species');
          requestBody.remove('created');
          requestBody.remove('edited');
          db.insert(
            'film',
            requestBody,
            nullColumnHack: null,
          );
        }
      }
    } catch (e) {
      Log.e("Exception PeopleService.insertFilmLocal : $e");
    }
  }

  // =====> Insert Favorite to Db Local <=====
  static Future insertFavorite(Map<String, dynamic> requestBody) async {
    try {
      Database? db = await DbHelper().database;

      List<Map<String, dynamic>> tempListFavorite = await getFavorite();

      // Insert Data to local
      if (db!.isOpen) {
        final tempFavorite = tempListFavorite
            .where((e) =>
                e['people_id'] == requestBody['people_id'] &&
                e['users_id'] == requestBody['users_id'])
            .toList();
        if (tempFavorite.isEmpty) {
          db.insert(
            'favorite',
            requestBody,
            nullColumnHack: null,
          );
          return true;
        }
      }
    } catch (e) {
      Log.e("Exception PeopleService.insertFavorite : $e");
      throw e;
    }
  }

  // =====> Insert UnFavorite to Db Local <=====
  static Future unFavorite(Map<String, dynamic> requestBody) async {
    try {
      Database? db = await DbHelper().database;

      // Insert Data to local
      if (db!.isOpen) {
        await db.rawDelete(
          'DELETE FROM favorite WHERE people_id=? and users_id=?',
          ["${requestBody['people_id']}", requestBody['users_id']],
        );
        return true;
      }
    } catch (e) {
      Log.e("Exception PeopleService.unFavorite : $e");
      throw e;
    }
  }
}
