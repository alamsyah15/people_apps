import 'package:people_apps/core/model/user_model.dart';
import 'package:people_apps/core/services/log.dart';
import 'package:people_apps/core/utils/db_helper.dart';
import 'package:sqflite/sqflite.dart';

class AuthServices {
  // =====> Signin Service <======
  static Future signIn(Map<String, dynamic> requestBody) async {
    try {
      Database? db = await DbHelper().database;
      if (db!.isOpen) {
        final result = await db.rawQuery(
          'SELECT * FROM users WHERE email=? and password=?',
          ['${requestBody['email']}', '${requestBody['password']}'],
        );
        if (result.isNotEmpty) {
          return UserModel.fromJson(result.first);
        } else {
          return "Email atau password salah!";
        }
      }
    } catch (e) {
      Log.e("Error AuthServices.signIn $e");
    }
  }

  static Future signUp(Map<String, dynamic> requestBody) async {
    try {
      Database? db = await DbHelper().database;
      if (db!.isOpen) {
        final result = await db.rawQuery(
          'SELECT * FROM users WHERE email=?',
          ['${requestBody['email']}'],
        );

        /// regis action
        if (result.isEmpty) {
          await db.insert('users', requestBody);
          return requestBody;
        } else {
          return "Email sudah terdaftar!";
        }
      }
    } catch (e) {
      Log.e("Error Auth SignUp $e");
    }
  }
}
