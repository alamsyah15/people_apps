// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables, sized_box_for_whitespace

import 'package:flutter/material.dart';
import 'package:people_apps/core/utils/constant.dart';
import 'package:people_apps/core/utils/session_manager.dart';
import 'package:people_apps/core/viewmodels/auth/auth_provider.dart';
import 'package:people_apps/screen/home/widget/double_text_widget.dart';
import 'package:provider/provider.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  @override
  Widget build(BuildContext context) {
    final user = Provider.of<AuthProvider>(context, listen: false).user;
    return Scaffold(
      appBar: AppBar(
        title: Text("Account"),
      ),
      body: Center(
        child: Padding(
          padding: EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Icon(Icons.person, size: 100),
              SizedBox(height: 24),
              Card(
                child: Padding(
                  padding: EdgeInsets.symmetric(vertical: 16.0, horizontal: 24),
                  child: Column(
                    children: [
                      DoubleTextWidget(
                        title: "Name",
                        content: "${user?.name}",
                        titleFlex: 1,
                      ),
                      DoubleTextWidget(
                        title: "Email",
                        content: "${user?.email}",
                        titleFlex: 1,
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: 24),
              Container(
                margin: EdgeInsets.symmetric(vertical: 16),
                width: widthScreen,
                height: 45,
                child: MaterialButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  color: Colors.black45,
                  textColor: Colors.white,
                  child: Text("Logout"),
                  onPressed: SessionManager.removeSessionLogin,
                ),
              ),
              SizedBox(height: 10),
            ],
          ),
        ),
      ),
    );
  }
}
