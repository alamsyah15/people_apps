import 'package:flutter/material.dart';
import 'package:people_apps/core/utils/constant.dart';
import 'package:people_apps/core/viewmodels/home/navigation_provider.dart';
import 'package:provider/provider.dart';

class HomeNavigation extends StatelessWidget {
  const HomeNavigation({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final navProv = Provider.of<NavigationProvider>(context, listen: true);
    return Scaffold(
      body: navProv.displayScreen,
      bottomNavigationBar: BottomNavigationBar(
        items: listNavMenu,
        selectedItemColor: Colors.amber,
        currentIndex: navProv.currentIndex,
        onTap: (index) {
          navProv.setDisplay(listScreen[index]);
          navProv.currentIndex = index;
        },
      ),
    );
  }
}
