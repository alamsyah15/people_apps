// ignore_for_file: prefer_const_constructors, avoid_unnecessary_containers, prefer_const_literals_to_create_immutables

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:people_apps/core/model/people_model.dart';
import 'package:people_apps/core/utils/constant.dart';
import 'package:people_apps/core/viewmodels/home/detail_people_provider.dart';
import 'package:people_apps/core/viewmodels/home/people_provider.dart';
import 'package:people_apps/screen/home/widget/double_text_widget.dart';
import 'package:people_apps/screen/home/widget/film_widget.dart';
import 'package:people_apps/screen/home/widget/spesies_widget.dart';
import 'package:provider/provider.dart';

class DetailScreen extends StatefulWidget {
  final Result people;
  const DetailScreen({Key? key, required this.people}) : super(key: key);

  @override
  _DetailScreenState createState() => _DetailScreenState();
}

class _DetailScreenState extends State<DetailScreen> {
  Result get people => widget.people;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    Provider.of<DetailPeopleProvider>(context, listen: false)
        .initialized(people);
  }

  @override
  Widget build(BuildContext context) {
    final peopleProv = Provider.of<DetailPeopleProvider>(context, listen: true);
    return Scaffold(
      appBar: AppBar(
        title: Text("Detail"),
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.all(16),
          child: Card(
            child: Container(
              padding: EdgeInsets.all(16),
              width: widthScreen,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 16),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "${people.name}",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 18),
                      ),
                      InkWell(
                        onTap: () => peopleProv.setFavorite(people.url!),
                        child: Icon(
                          peopleProv.isFavorite
                              ? Icons.bookmark
                              : Icons.bookmark_outline_outlined,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 16),
                  Text(
                    "Deskripsi",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                  ),
                  Divider(thickness: 1),
                  Row(
                    children: [
                      Flexible(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(height: 16),
                            DoubleTextWidget(
                              title: "Height",
                              content: "${people.height}cm",
                            ),
                            SizedBox(height: 10),
                            DoubleTextWidget(
                              title: "Mass",
                              content: "${people.mass}kg",
                            ),
                            SizedBox(height: 10),
                            DoubleTextWidget(
                              title: "Eye Color",
                              content: "${people.eyeColor}",
                            ),
                          ],
                        ),
                      ),
                      Flexible(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(height: 16),
                            DoubleTextWidget(
                              title: "Birth Year",
                              content: people.birthYear,
                            ),
                            SizedBox(height: 10),
                            DoubleTextWidget(
                              title: "Gender",
                              content: "${people.gender}",
                            ),
                            SizedBox(height: 10),
                            DoubleTextWidget(
                              title: "Skin Color",
                              content: "${people.skinColor}",
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 16),
                  SpesiesWidget(
                    noRecordData: peopleProv.spesiesData == null,
                    spesiesData: peopleProv.spesiesData,
                  ),
                  SizedBox(height: 16),
                  FilmWidget(
                    noRecordData: peopleProv.listFilm.isEmpty,
                    listFilm: peopleProv.listFilm,
                  ),
                  SizedBox(height: 16),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
