// ignore_for_file: avoid_unnecessary_containers, prefer_const_literals_to_create_immutables, prefer_const_constructors, sized_box_for_whitespace

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:people_apps/core/model/people_model.dart';
import 'package:people_apps/core/services/log.dart';
import 'package:people_apps/core/utils/constant.dart';
import 'package:people_apps/core/utils/navigation_helper.dart';
import 'package:people_apps/core/viewmodels/home/people_provider.dart';
import 'package:people_apps/screen/home/detail_screen.dart';
import 'package:people_apps/screen/home/widget/custom_textfield.dart';
import 'package:provider/provider.dart';

class ManagePeople extends StatefulWidget {
  final Result? people;
  const ManagePeople({Key? key, this.people}) : super(key: key);
  @override
  _ManagePeopleState createState() => _ManagePeopleState();
}

class _ManagePeopleState extends State<ManagePeople> {
  Result? people = Result();
  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  void submit() async {
    final validate = formKey.currentState?.validate();
    final peopleProv = Provider.of<PeopleProvider>(context, listen: false);
    Log.v("Result $validate");

    if (validate == true) {
      // Show dialog confirm
      final result = await showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              title: Text("Information!!"),
              content: Text("Do you want to submit this data?"),
              actions: [
                MaterialButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(4),
                  ),
                  color: Colors.white24,
                  textColor: Colors.white,
                  child: Text("Submit"),
                  onPressed: () => pushBack(true),
                ),
                MaterialButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(4),
                  ),
                  color: Colors.black,
                  textColor: Colors.white,
                  child: Text("Cancel"),
                  onPressed: () => pushBack(false),
                ),
              ],
            );
          });

      // Trigger action
      if (result == true) {
        if (widget.people != null) {
          // Action Update
          peopleProv.updatePeople(people!);
        } else {
          // Action Insert
          peopleProv.insertPeople(people!);
        }
      }
    }
  }

  void delete() async {
    final peopleProv = Provider.of<PeopleProvider>(context, listen: false);
    if (widget.people != null) {
      final result = await showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              title: Text("Information!!"),
              content: Text("Do you want to delete this data?"),
              actions: [
                MaterialButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(4),
                  ),
                  color: Colors.white24,
                  textColor: Colors.white,
                  child: Text("Yes"),
                  onPressed: () => pushBack(true),
                ),
                MaterialButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(4),
                  ),
                  color: Colors.black,
                  textColor: Colors.white,
                  child: Text("Cancel"),
                  onPressed: () => pushBack(false),
                ),
              ],
            );
          });
      if (result == true) {
        peopleProv.deletePeople(people!);
      }
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (widget.people != null) {
      people = widget.people;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.people != null ? "Edit People" : "Add People"),
      ),
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.all(16),
          child: Form(
            key: formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Form Data",
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                    Visibility(
                      visible:
                          widget.people != null && widget.people?.url != null,
                      child: Align(
                        alignment: Alignment.topRight,
                        child: InkWell(
                          onTap: () {
                            pushTo(DetailScreen(people: people!));
                          },
                          child: Container(
                            margin: EdgeInsets.all(16),
                            padding: EdgeInsets.symmetric(
                                vertical: 8, horizontal: 16),
                            child: Text("Lihat Detail"),
                            decoration: BoxDecoration(
                              color: Colors.black45,
                              borderRadius: BorderRadius.circular(10),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 16),
                CustomTextField(
                  initialValue: people?.name,
                  labelText: 'Name',
                  hintText: 'John doe',
                  onChange: (String value) => people?.name = value,
                ),
                SizedBox(height: 16),
                Row(
                  children: [
                    Expanded(
                      child: CustomTextField(
                        initialValue: people?.height,
                        labelText: 'Height',
                        hintText: '172',
                        onChange: (String value) => people?.height = value,
                      ),
                    ),
                    SizedBox(width: 16),
                    Expanded(
                      child: CustomTextField(
                        initialValue: people?.mass,
                        labelText: 'Mass',
                        hintText: '80',
                        onChange: (String value) => people?.mass = value,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 16),
                Row(
                  children: [
                    Expanded(
                      child: CustomTextField(
                        initialValue: people?.hairColor,
                        labelText: 'Hair Color',
                        hintText: 'Red',
                        onChange: (String value) => people?.hairColor = value,
                      ),
                    ),
                    SizedBox(width: 16),
                    Expanded(
                      child: CustomTextField(
                        initialValue: people?.skinColor,
                        labelText: 'Skin Color',
                        hintText: 'Red',
                        onChange: (String value) => people?.skinColor = value,
                      ),
                    ),
                    SizedBox(width: 16),
                    Expanded(
                      child: CustomTextField(
                        initialValue: people?.eyeColor,
                        labelText: 'Eye Color',
                        hintText: 'Red',
                        onChange: (String value) => people?.eyeColor = value,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 16),
                CustomTextField(
                  initialValue: people?.birthYear,
                  labelText: 'Birth Year',
                  hintText: '2000',
                  onChange: (String value) => people?.birthYear = value,
                ),
                SizedBox(height: 16),
                CustomTextField(
                  initialValue: people?.gender,
                  labelText: 'Gender',
                  hintText: 'Male',
                  onChange: (String value) => people?.gender = value,
                ),
                Spacer(),
                Row(
                  children: [
                    Expanded(
                      child: Container(
                        width: widthScreen,
                        height: 45,
                        child: MaterialButton(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                          color: Colors.white24,
                          textColor: Colors.white,
                          child: Text(
                            widget.people != null
                                ? "Update Data"
                                : "Insert Data",
                          ),
                          onPressed: submit,
                        ),
                      ),
                    ),
                    Visibility(
                        visible: widget.people != null,
                        child: SizedBox(width: 16)),
                    Visibility(
                      visible: widget.people != null,
                      child: Expanded(
                        child: Container(
                          width: widthScreen,
                          height: 45,
                          child: MaterialButton(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                            color: Colors.amber,
                            textColor: Colors.black,
                            child: Text("Delete Data"),
                            onPressed: delete,
                          ),
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
