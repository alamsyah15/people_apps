// ignore_for_file: avoid_unnecessary_containers, prefer_const_literals_to_create_immutables, prefer_const_constructors, unnecessary_string_interpolations

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:people_apps/core/model/people_model.dart';
import 'package:people_apps/screen/home/widget/double_text_widget.dart';

class ItemListView extends StatelessWidget {
  final Result people;
  final Function onTap;
  const ItemListView({Key? key, required this.people, required this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        onTap();
      },
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 8),
        child: Card(
          elevation: 4,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadiusDirectional.circular(10),
          ),
          child: Container(
            padding: EdgeInsets.all(16),
            child: ListTile(
              title: Text(
                "${people.name}",
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              subtitle: Row(
                children: [
                  Flexible(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(height: 16),
                        DoubleTextWidget(
                          title: "Height",
                          content: "${people.height}cm",
                        ),
                        DoubleTextWidget(
                          title: "Mass",
                          content: "${people.mass}kg",
                        ),
                      ],
                    ),
                  ),
                  Flexible(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(height: 16),
                        DoubleTextWidget(
                          title: "Birth Year",
                          content: people.birthYear,
                        ),
                        DoubleTextWidget(
                          title: "Gender",
                          content: "${people.gender}",
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
