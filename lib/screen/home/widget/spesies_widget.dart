// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:people_apps/core/model/spesies_model.dart';
import 'package:people_apps/screen/home/widget/double_text_widget.dart';

class SpesiesWidget extends StatelessWidget {
  final bool noRecordData;
  final SpesiesModel? spesiesData;
  const SpesiesWidget(
      {Key? key, required this.noRecordData, required this.spesiesData})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 16),
        Text(
          "Spesies ${spesiesData?.name ?? ""}",
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
        ),
        Divider(thickness: 1),
        noRecordData
            ? Text("No record data", style: TextStyle(fontSize: 16))
            : Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Flexible(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(height: 16),
                        DoubleTextWidget(
                          title: "Classification",
                          content: "${spesiesData?.classification}",
                        ),
                        SizedBox(height: 10),
                        DoubleTextWidget(
                          title: "Designation",
                          content: "${spesiesData?.designation}",
                        ),
                        SizedBox(height: 10),
                        DoubleTextWidget(
                          title: "Average Height",
                          content: "${spesiesData?.averageHeight}",
                        ),
                      ],
                    ),
                  ),
                  Flexible(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        SizedBox(height: 16),
                        DoubleTextWidget(
                          title: "Skin Colors",
                          content: "${spesiesData?.skinColors}",
                        ),
                        SizedBox(height: 10),
                        DoubleTextWidget(
                          title: "Hair Colors",
                          content: "${spesiesData?.hairColors}",
                        ),
                        SizedBox(height: 10),
                        DoubleTextWidget(
                          title: "Eye Colors",
                          content: "${spesiesData?.eyeColors}",
                        ),
                      ],
                    ),
                  ),
                ],
              ),
      ],
    );
  }
}
