// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:people_apps/core/model/people_model.dart';
import 'package:people_apps/screen/home/widget/double_text_widget.dart';

class ItemGridView extends StatelessWidget {
  final Result people;
  final Function onTap;
  const ItemGridView({Key? key, required this.people, required this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        onTap();
      },
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Card(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          elevation: 4,
          clipBehavior: Clip.antiAlias,
          child: Container(
            padding: EdgeInsets.all(16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "${people.name}",
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                ),
                SizedBox(height: 16),
                Text(
                  "Deskripsi",
                  style: TextStyle(
                    fontWeight: FontWeight.w700,
                  ),
                ),
                SizedBox(height: 16),
                DoubleTextWidget(
                    title: "Birth Year", content: "${people.birthYear}"),
                DoubleTextWidget(title: "Gender", content: "${people.gender}"),
                Divider(thickness: 1),
                DoubleTextWidget(
                    title: "Height", content: "${people.height}cm"),
                DoubleTextWidget(title: "Mass", content: "${people.mass}kg"),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
