// ignore_for_file: override_on_non_overriding_member, prefer_const_constructors

import 'package:flutter/material.dart';

class DoubleTextWidget extends StatelessWidget {
  final String? title, content;
  final int? titleFlex, contentFlex;
  const DoubleTextWidget(
      {Key? key,
      required this.title,
      required this.content,
      this.titleFlex,
      this.contentFlex})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
          flex: titleFlex ?? 4,
          child: Text(
            "$title",
            style: TextStyle(color: Colors.amber),
          ),
        ),
        Text(": "),
        Expanded(
          flex: contentFlex ?? 3,
          child: Text("$content"),
        )
      ],
    );
  }
}
