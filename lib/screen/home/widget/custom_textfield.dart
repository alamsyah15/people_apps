// ignore_for_file: prefer_const_constructors, avoid_unnecessary_containers

import 'package:flutter/material.dart';

class CustomTextField extends StatelessWidget {
  final String? initialValue, labelText, hintText;
  final Function(String value) onChange;
  const CustomTextField(
      {Key? key,
      required this.initialValue,
      required this.onChange,
      this.labelText,
      this.hintText})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: TextFormField(
        initialValue: initialValue,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return "Form wajib diisi!";
          }
          return null;
        },
        onChanged: onChange,
        decoration: InputDecoration(
          border: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.teal),
          ),
          hintText: hintText ?? '',
          labelText: labelText ?? '',
        ),
      ),
    );
  }
}
