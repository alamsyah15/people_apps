// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables, avoid_unnecessary_containers, sized_box_for_whitespace

import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/widgets.dart';
import 'package:people_apps/core/model/film_model.dart';
import 'package:people_apps/core/utils/constant.dart';
import 'package:people_apps/screen/home/widget/double_text_widget.dart';

class FilmWidget extends StatelessWidget {
  final bool noRecordData;
  final List<FilmModel> listFilm;
  const FilmWidget(
      {Key? key, required this.noRecordData, required this.listFilm})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 16),
        Text(
          "Film ",
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
        ),
        Divider(thickness: 1),
        noRecordData
            ? Text("No record data", style: TextStyle(fontSize: 16))
            : ListView.builder(
                shrinkWrap: true,
                itemCount: listFilm.length,
                physics: NeverScrollableScrollPhysics(),
                itemBuilder: (contex, index) {
                  final film = listFilm[index];
                  return Container(
                    width: widthScreen,
                    padding: EdgeInsets.all(16),
                    margin: EdgeInsets.only(top: 10),
                    decoration: BoxDecoration(
                      color: Colors.black45,
                      borderRadius: BorderRadius.circular(8),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          "${film.title}",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 16,
                          ),
                        ),
                        SizedBox(height: 16),
                        DoubleTextWidget(
                          title: "Total Episode",
                          titleFlex: 2,
                          content: "${film.episodeId}",
                        ),
                        SizedBox(height: 10),
                        DoubleTextWidget(
                          title: "Director",
                          titleFlex: 2,
                          content: "${film.director}",
                        ),
                        SizedBox(height: 10),
                        DoubleTextWidget(
                          title: "Producer",
                          titleFlex: 2,
                          content: "${film.producer}",
                        ),
                        SizedBox(height: 16),
                        Text(
                          "Synopsis : ",
                          style: TextStyle(
                            color: Colors.amber,
                            fontWeight: FontWeight.w700,
                            fontSize: 14,
                          ),
                        ),
                        Text(
                          "${film.openingCrawl}",
                          textAlign: TextAlign.justify,
                        )
                      ],
                    ),
                  );
                },
              )
      ],
    );
  }
}
