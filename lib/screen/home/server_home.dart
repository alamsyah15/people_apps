// ignore_for_file: prefer_const_constructors

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:people_apps/core/utils/navigation_helper.dart';
import 'package:people_apps/core/viewmodels/home/navigation_provider.dart';
import 'package:people_apps/core/viewmodels/home/people_provider.dart';
import 'package:people_apps/screen/home/detail_screen.dart';
import 'package:people_apps/screen/home/widget/item_grid_view.dart';
import 'package:people_apps/screen/home/widget/item_list_view.dart';
import 'package:provider/provider.dart';

class ServerHome extends StatefulWidget {
  const ServerHome({Key? key}) : super(key: key);

  @override
  _ServerHomeState createState() => _ServerHomeState();
}

class _ServerHomeState extends State<ServerHome> {
  @override
  Widget build(BuildContext context) {
    final navProv = Provider.of<NavigationProvider>(context, listen: true);
    final peopleProv = Provider.of<PeopleProvider>(context, listen: true);
    return peopleProv.isLoading
        ? Center(child: CupertinoActivityIndicator())
        : Container(
            padding: EdgeInsets.symmetric(vertical: 16),
            child: Column(
              children: [
                Expanded(
                  child: navProv.isGridView
                      ? GridView.builder(
                          controller: peopleProv.scController,
                          gridDelegate:
                              SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 2,
                            childAspectRatio: 0.80,
                          ),
                          itemCount: peopleProv.listPeople.length,
                          itemBuilder: (context, index) {
                            final people = peopleProv.listPeople[index];
                            return ItemGridView(
                              people: people,
                              onTap: () {
                                pushTo(DetailScreen(people: people));
                              },
                            );
                          },
                        )
                      : ListView.builder(
                          controller: peopleProv.scController,
                          physics: AlwaysScrollableScrollPhysics(),
                          itemCount: peopleProv.listPeople.length,
                          itemBuilder: (context, index) {
                            final people = peopleProv.listPeople[index];

                            return ItemListView(
                              people: people,
                              onTap: () {
                                pushTo(DetailScreen(people: people));
                              },
                            );
                          },
                        ),
                ),
                Visibility(
                  visible: peopleProv.isLoadMore,
                  child: Padding(
                    padding: EdgeInsets.only(top: 8.0),
                    child: Text("Load More data ....."),
                  ),
                ),
              ],
            ),
          );
  }
}
