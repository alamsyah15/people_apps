// ignore_for_file: prefer_const_constructors, avoid_unnecessary_containers, prefer_const_literals_to_create_immutables, unnecessary_string_interpolations

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:people_apps/core/utils/constant.dart';
import 'package:people_apps/core/utils/navigation_helper.dart';
import 'package:people_apps/core/viewmodels/home/navigation_provider.dart';
import 'package:people_apps/core/viewmodels/home/people_provider.dart';
import 'package:people_apps/screen/home/local_home.dart';
import 'package:people_apps/screen/home/server_home.dart';
import 'package:people_apps/screen/home/widget/manage_people.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with TickerProviderStateMixin {
  late TabController tabController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    tabController = TabController(length: 2, vsync: this);

    Provider.of<PeopleProvider>(context, listen: false).initialized();
  }

  @override
  Widget build(BuildContext context) {
    final navProv = Provider.of<NavigationProvider>(context, listen: true);
    final peopleProv = Provider.of<PeopleProvider>(context, listen: true);

    return Scaffold(
      appBar: AppBar(
        title: CupertinoSearchTextField(
          backgroundColor: Colors.white30,
          placeholderStyle: TextStyle(color: Colors.white),
          prefixInsets: EdgeInsets.symmetric(horizontal: 10),
          style: TextStyle(color: Colors.white),
          onChanged: peopleProv.search,
        ),
        actions: [
          PopupMenuButton(
            initialValue: peopleProv.valueFilter,
            child: Icon(Icons.sort),
            onSelected: peopleProv.filterBy,
            itemBuilder: (context) {
              return listFilter
                  .asMap()
                  .map((i, value) => MapEntry(
                      i,
                      PopupMenuItem(
                        child: Text("$value"),
                        value: i,
                      )))
                  .values
                  .toList();
            },
          ),
          IconButton(
            onPressed: () {
              navProv.isGridView = !navProv.isGridView;
            },
            icon: Icon(
              navProv.isGridView ? Icons.list_sharp : Icons.grid_on_rounded,
            ),
          ),
        ],
      ),
      body: LocalHome(),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.white,
        child: Icon(Icons.add),
        onPressed: () {
          pushTo(ManagePeople());
        },
      ),
    );
  }
}
