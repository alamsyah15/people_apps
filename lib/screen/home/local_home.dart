// ignore_for_file: prefer_const_constructors

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:people_apps/core/utils/navigation_helper.dart';
import 'package:people_apps/core/viewmodels/home/navigation_provider.dart';
import 'package:people_apps/core/viewmodels/home/people_provider.dart';
import 'package:people_apps/screen/home/detail_screen.dart';
import 'package:people_apps/screen/home/widget/item_grid_view.dart';
import 'package:people_apps/screen/home/widget/item_list_view.dart';
import 'package:people_apps/screen/home/widget/manage_people.dart';
import 'package:provider/provider.dart';

class LocalHome extends StatelessWidget {
  const LocalHome({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final navProv = Provider.of<NavigationProvider>(context, listen: true);
    final peopleProv = Provider.of<PeopleProvider>(context, listen: true);
    return peopleProv.isLoading
        ? Center(child: CupertinoActivityIndicator())
        : peopleProv.listPeopleLocal.isEmpty
            ? Center(child: Text("No data record"))
            : Container(
                padding: EdgeInsets.symmetric(vertical: 16),
                child: Column(
                  children: [
                    Expanded(
                      child: navProv.isGridView
                          ? GridView.builder(
                              controller: peopleProv.scController,
                              physics: AlwaysScrollableScrollPhysics(
                                parent: BouncingScrollPhysics(),
                              ),
                              gridDelegate:
                                  SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: 2,
                                childAspectRatio: 0.80,
                              ),
                              itemCount: peopleProv.listPeopleLocal.length,
                              itemBuilder: (context, index) {
                                final people =
                                    peopleProv.listPeopleLocal[index];
                                return ItemGridView(
                                  people: people,
                                  onTap: () {
                                    pushTo(ManagePeople(people: people));
                                  },
                                );
                              },
                            )
                          : ListView.builder(
                              controller: peopleProv.scController,
                              physics: AlwaysScrollableScrollPhysics(
                                parent: BouncingScrollPhysics(),
                              ),
                              itemCount: peopleProv.listPeopleLocal.length,
                              itemBuilder: (context, index) {
                                final people =
                                    peopleProv.listPeopleLocal[index];

                                return ItemListView(
                                  people: people,
                                  onTap: () {
                                    pushTo(ManagePeople(people: people));
                                  },
                                );
                              },
                            ),
                    ),
                    Visibility(
                      visible: peopleProv.isLoadMore,
                      child: Padding(
                        padding: EdgeInsets.only(top: 8.0),
                        child: Text("Load More data ....."),
                      ),
                    ),
                  ],
                ),
              );
  }
}
