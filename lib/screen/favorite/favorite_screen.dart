// ignore_for_file: prefer_const_constructors

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:people_apps/core/model/people_model.dart';
import 'package:people_apps/core/services/favorite/favorite_service.dart';
import 'package:people_apps/core/services/home/people_service.dart';
import 'package:people_apps/core/services/log.dart';
import 'package:people_apps/core/utils/db_helper.dart';
import 'package:people_apps/core/utils/navigation_helper.dart';
import 'package:people_apps/core/viewmodels/favorite/favorite_provider.dart';
import 'package:people_apps/screen/home/detail_screen.dart';
import 'package:people_apps/screen/home/widget/item_grid_view.dart';
import 'package:provider/provider.dart';

class FavoriteScreen extends StatefulWidget {
  const FavoriteScreen({Key? key}) : super(key: key);

  @override
  _FavoriteScreenState createState() => _FavoriteScreenState();
}

class _FavoriteScreenState extends State<FavoriteScreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Provider.of<FavoriteProvider>(context, listen: false).initilized();
  }

  @override
  Widget build(BuildContext context) {
    final favoriteProv = Provider.of<FavoriteProvider>(context, listen: true);
    return Scaffold(
      appBar: AppBar(
        title: Text("Favorite People"),
      ),
      body: favoriteProv.isLoading
          ? Center(child: CupertinoActivityIndicator())
          : favoriteProv.listPeople.isEmpty
              ? Center(child: Text("No data record"))
              : GridView.builder(
                  physics: AlwaysScrollableScrollPhysics(
                    parent: BouncingScrollPhysics(),
                  ),
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    childAspectRatio: 0.80,
                  ),
                  itemCount: favoriteProv.listPeople.length,
                  itemBuilder: (BuildContext context, int index) {
                    final people = favoriteProv.listPeople[index];
                    return ItemGridView(
                      people: people,
                      onTap: () {
                        pushTo(DetailScreen(people: people)).then((value) {
                          favoriteProv.initilized();
                        });
                      },
                    );
                  },
                ),
    );
  }
}
