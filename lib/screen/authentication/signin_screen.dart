// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:people_apps/core/utils/constant.dart';
import 'package:people_apps/core/utils/navigation_helper.dart';
import 'package:people_apps/core/utils/session_manager.dart';
import 'package:people_apps/core/viewmodels/auth/auth_provider.dart';
import 'package:people_apps/screen/authentication/siginup_screen.dart';
import 'package:provider/provider.dart';

class SigninScreen extends StatefulWidget {
  const SigninScreen({Key? key}) : super(key: key);

  @override
  _SigninScreenState createState() => _SigninScreenState();
}

class _SigninScreenState extends State<SigninScreen> {
  TextEditingController etEmail = TextEditingController();
  TextEditingController etPassword = TextEditingController();
  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  void login() {
    final authProv = Provider.of<AuthProvider>(context, listen: false);
    if (formKey.currentState?.validate() == true) {
      Map<String, dynamic> requestBody = {
        "email": etEmail.text,
        "password": etPassword.text
      };
      authProv.login(requestBody);
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback((timeStamp) async {
      Provider.of<AuthProvider>(context, listen: false).checkSession();
    });
  }

  @override
  Widget build(BuildContext context) {
    final authProv = Provider.of<AuthProvider>(context, listen: true);
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: SingleChildScrollView(
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 16),
              child: Card(
                elevation: 4,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Container(
                  padding: EdgeInsets.all(16),
                  width: widthScreen,
                  child: Form(
                    key: formKey,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        SizedBox(height: 16),
                        Icon(Icons.person, size: 64),
                        Text("SignIn Account"),
                        SizedBox(height: 16),
                        TextFormField(
                          controller: etEmail,
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return "Form wajib diisi!";
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.teal),
                            ),
                            hintText: 'example@mail.com',
                            labelText: 'Email',
                            prefixIcon:
                                Icon(Icons.email, color: Colors.white30),
                          ),
                        ),
                        SizedBox(height: 16),
                        TextFormField(
                          controller: etPassword,
                          obscureText: authProv.hidePassword,
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return "Form wajib diisi!";
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.teal),
                            ),
                            hintText: '*****',
                            labelText: 'Password',
                            prefixIcon: Icon(Icons.lock, color: Colors.white30),
                            suffixIcon: InkWell(
                              onTap: authProv.setHidePassword,
                              child: Icon(
                                authProv.hidePassword
                                    ? Icons.visibility
                                    : Icons.visibility_off,
                              ),
                            ),
                          ),
                        ),
                        Row(
                          children: [
                            Flexible(
                              child: Container(
                                margin: EdgeInsets.symmetric(vertical: 16),
                                width: widthScreen,
                                height: 45,
                                child: MaterialButton(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  color: Colors.black45,
                                  textColor: Colors.white,
                                  child: Text("Sign In"),
                                  onPressed: login,
                                ),
                              ),
                            ),
                            SizedBox(width: 16),
                            Flexible(
                              child: Container(
                                width: widthScreen,
                                height: 45,
                                child: MaterialButton(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  color: Colors.white24,
                                  textColor: Colors.white,
                                  child: Text("Sign Up"),
                                  onPressed: () {
                                    pushTo(SignupScreen());
                                  },
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 16),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
