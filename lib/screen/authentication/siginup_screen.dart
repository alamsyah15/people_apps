// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables, unnecessary_null_comparison

import 'package:flutter/material.dart';
import 'package:people_apps/core/services/auth/auth_services.dart';
import 'package:people_apps/core/services/log.dart';
import 'package:people_apps/core/utils/constant.dart';
import 'package:people_apps/core/utils/navigation_helper.dart';
import 'package:people_apps/core/viewmodels/auth/auth_provider.dart';
import 'package:provider/provider.dart';

class SignupScreen extends StatefulWidget {
  const SignupScreen({Key? key}) : super(key: key);

  @override
  _SignupScreenState createState() => _SignupScreenState();
}

class _SignupScreenState extends State<SignupScreen> {
  TextEditingController etName = TextEditingController();
  TextEditingController etEmail = TextEditingController();
  TextEditingController etPassword = TextEditingController();
  GlobalKey<FormState> keyForm = GlobalKey<FormState>();

  void register() {
    final authProv = Provider.of<AuthProvider>(context, listen: false);

    if (keyForm.currentState?.validate() == true) {
      Map<String, dynamic> requestBody = {
        "name": etName.text,
        "email": etEmail.text,
        "password": etPassword.text
      };
      authProv.register(requestBody);
    }
  }

  @override
  void dispose() {
    etName.dispose();
    etEmail.dispose();
    etPassword.dispose();
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final authProv = Provider.of<AuthProvider>(context, listen: true);
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: SingleChildScrollView(
            child: Container(
              alignment: Alignment.center,
              margin: EdgeInsets.symmetric(horizontal: 16),
              child: Card(
                elevation: 4,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Container(
                  padding: EdgeInsets.all(16),
                  width: widthScreen,
                  child: Form(
                    key: keyForm,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        SizedBox(height: 16),
                        Icon(Icons.person, size: 64),
                        Text("SignUp Account"),
                        SizedBox(height: 16),
                        TextFormField(
                          controller: etName,
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return "Form wajib diisi!";
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.teal),
                            ),
                            hintText: 'John Doe',
                            labelText: 'Name',
                            prefixIcon:
                                Icon(Icons.email, color: Colors.white30),
                          ),
                        ),
                        SizedBox(height: 16),
                        TextFormField(
                          controller: etEmail,
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return "Form wajib diisi!";
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.teal),
                            ),
                            hintText: 'example@mail.com',
                            labelText: 'Email',
                            prefixIcon:
                                Icon(Icons.email, color: Colors.white30),
                          ),
                        ),
                        SizedBox(height: 16),
                        TextFormField(
                          controller: etPassword,
                          obscureText: authProv.hidePassword,
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return "Form wajib diisi!";
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.teal),
                            ),
                            hintText: '*****',
                            labelText: 'Password',
                            prefixIcon: Icon(Icons.lock, color: Colors.white30),
                            suffixIcon: InkWell(
                              onTap: authProv.setHidePassword,
                              child: Icon(
                                authProv.hidePassword
                                    ? Icons.visibility
                                    : Icons.visibility_off,
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: 24),
                        Container(
                          width: widthScreen,
                          height: 45,
                          child: MaterialButton(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                            color: Colors.white24,
                            textColor: Colors.white,
                            child: Text("Sign Up"),
                            onPressed: register,
                          ),
                        ),
                        SizedBox(height: 10),
                        InkWell(
                          onTap: () => pushBack(),
                          child: Text("Already have an account?"),
                        ),
                        SizedBox(height: 16),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
